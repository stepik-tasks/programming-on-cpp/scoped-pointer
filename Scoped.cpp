struct Expression;
struct Number;
struct BinaryOperation;

struct ScopedPtr
{
    explicit ScopedPtr(Expression *ptr = 0) : ptr_(ptr)
    {};
    ~ScopedPtr()
    {
        if (ptr_)
            delete ptr_;
    }
    
    Expression* release()
    {
        Expression* old = this->ptr_;
        this->ptr_ = 0;
        return old;
    }
    
    Expression* get() const
    {
        return this->ptr_;
    }
    
    void reset(Expression *ptr = 0)
    {
        delete this->ptr_;
        this->ptr_ = ptr;
    }
    
    Expression& operator*() const
    {
        return *(this->ptr_);
    }

    Expression* operator->() const
    {
        return this->ptr_;
    }
private:
    // запрещаем копирование ScopedPtr
    ScopedPtr(const ScopedPtr&);
    ScopedPtr& operator=(const ScopedPtr&);

    Expression *ptr_;
};